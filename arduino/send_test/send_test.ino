#include <SPI.h>
#include <RF24.h>
RF24 radio(10, 6); // можно использовать любые
byte address[] = { 0xCC,0xCC,0xCC,0xCC,0xCA };
byte massiv[1];

void setup()
{
  Serial.begin(57600);
  radio.begin();
  radio.setDataRate(RF24_250KBPS); // скорость обмена данными RF24_1MBPS или RF24_2MBPS
  radio.openWritingPipe(address); // открыть канал на отправку
}


void loop()   
{
  massiv[0] = 255;
  radio.write(massiv, 1);
  delay(1000);
  massiv[0] = 155;
  radio.write(massiv, 1);
  delay(1000);
}

