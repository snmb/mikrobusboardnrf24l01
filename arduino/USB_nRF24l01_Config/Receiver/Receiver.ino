//--------------RECEIVER--------------------//

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define CE_PIN 10
#define CSN_PIN 6
uint8_t TARGET_ADDRESS[] = {0x0F,0x0F,0x0F,0x0F,0xD2}; //Destination address(USB Adapter)
uint8_t LOCAL_ADDRESS[] = {0x0F,0x0F,0x0F,0x0F,0xE1}; //Set arduino address
#define CHANNEL 4 // 0-125 in this case-> 2400+4=2404
#define RF_POWER_LEVEL RF24_PA_LOW // RF24_PA_MIN, RF24_PA_LOW, RF24_PA_HIGH and RF24_PA_MAX
#define INTERVAL 500 // Timeout Interval

#define USE_USB_ADAPTER 1

struct struct_example{
    char buffer_size = 31;
    char char_1 = -30;
    char char_2 = 120;
    uint8_t uint8_t_1 = 255;
    uint8_t uint8_t_2 = 0;
    int int_1 = 10200;
    int int_2 = -3000;
    float float_1 = 18.43f;
    float float_2 = -2343.212f;    
  };

RF24 radio(9,10);

void setup(void)
   {
    Serial.begin(115200); 
    SPI.begin();
    radio.begin();
    radio.setRetries(15,15);
    radio.setDataRate( RF24_250KBPS );
    radio.setChannel(CHANNEL);
    radio.setPALevel(RF_POWER_LEVEL);
    radio.openWritingPipe(TARGET_ADDRESS);
    radio.openReadingPipe(1,LOCAL_ADDRESS);
    radio.startListening();
   }

void loop(){
  unsigned long t_transcurrido = millis();
  bool timeout = false;

  while ( ! radio.available() && ! timeout )       // Wait INTERVAL (500ms)
        if (millis() - t_transcurrido > INTERVAL )
            timeout = true;

  if ( timeout )
       Serial.println("Waiting data...");
  else
   {   
       unsigned char r_buffer [31];
       radio.read( &r_buffer, sizeof(r_buffer)+1); // Read data received

       Serial.println((char*)r_buffer);

        radio.stopListening();        // stop Listening to Send data
       
        struct_example msg;
        radio.write( &msg , sizeof(msg));
        Serial.println("Sending Data");

       // Volvemos a la escucha para recibir mas paquetes
       radio.startListening();
   }
}
