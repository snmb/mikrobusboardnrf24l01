#include "snnrf24.h"

snnrf24 snnrf = snnrf24();

snnrf24::snnrf24() {
	cePin = 10;
	csnPin = 6;
	channel = 4;
	payload = 32;
    raddress = "AAAAB";
    taddress = "AAAAA";
}

void snnrf24::init() {   
    pinMode(cePin,OUTPUT);
    pinMode(csnPin,OUTPUT);
    digitalWrite(cePin,HIGH);
    digitalWrite(csnPin,HIGH);
    // Initialize spi module
    SPI.begin();
	SPI.setDataMode(SPI_MODE0);
	SPI.setClockDivider(SPI_2XCLOCK_MASK);
}

void snnrf24::config() {
   // digitalWrite(csnPin,LOW);
	configRegister(RX_PW_P0, payload);

    digitalWrite(csnPin,LOW);
    SPI.transfer(FLUSH_RX);
    digitalWrite(csnPin,HIGH);

    digitalWrite(csnPin,LOW);
    SPI.transfer(NOP);
    digitalWrite(csnPin,HIGH);

    writeRegister(TX_ADDR, taddress, 5);
    
    writeRegister(RX_ADDR_P0, raddress, 5);

    digitalWrite(csnPin,LOW);
	configRegister(EN_AA, ENAA_P1);
    digitalWrite(csnPin,HIGH);

    digitalWrite(csnPin,LOW);
	configRegister(EN_RXADDR, ENAA_P1);
    digitalWrite(csnPin,HIGH);

    digitalWrite(csnPin,LOW);
	configRegister(SETUP_RETR, SETUP_RETR_DELAY_500MKS | SETUP_RETR_UP_TO_10_RETRANSMIT);
    digitalWrite(csnPin,HIGH);

    digitalWrite(csnPin,LOW);
	configRegister(RF_CH, channel);
    digitalWrite(csnPin,HIGH);

    digitalWrite(csnPin,LOW);
	configRegister(RF_SETUP, RF_PWR_12 | RF_DR_LOW | LNA_HCURR);
    digitalWrite(csnPin,HIGH);   

    digitalWrite(csnPin,LOW);
	configRegister(CONFIG, EN_CRC | CRCO | PWR_UP | PRIM_RX);
    digitalWrite(csnPin,HIGH);   
}

void snnrf24::configRegister(uint8_t reg, uint8_t value) {
// Clocks only one byte into the given MiRF register
    digitalWrite(csnPin,LOW);
    SPI.transfer(W_REGISTER | (0x1F & reg));
    SPI.transfer(value);
    digitalWrite(csnPin,HIGH);
}

void snnrf24::transmitSync(uint8_t *dataout,uint8_t len) {
   	uint8_t i;
	for(i = 0;i < len; i ++){
		SPI.transfer(dataout[i]);
	} 
}

void snnrf24::writeRegister(uint8_t reg, uint8_t * value, uint8_t len) {
    digitalWrite(csnPin,LOW);
    SPI.transfer(W_REGISTER | (0x1F & reg));
    transmitSync(value, len);
    digitalWrite(csnPin,HIGH);
}

void snnrf24::send(uint8_t *value, uint8_t len) {
    digitalWrite(cePin,LOW);

    // digitalWrite(csnPin,LOW);
    // SPI.transfer(FLUSH_RX);
    // digitalWrite(csnPin,HIGH);

    digitalWrite(csnPin,LOW);
	configRegister(CONFIG, EN_CRC | CRCO | PWR_UP);
    digitalWrite(csnPin,HIGH);  

    digitalWrite(cePin,HIGH);

    digitalWrite(cePin,LOW);

    digitalWrite(csnPin,LOW);
    SPI.transfer(W_TX_PAYLOAD);
    SPI.transfer(len);
    transmitSync(value, len);
    for (int i = len; i < 31; i ++) SPI.transfer(0);
    digitalWrite(csnPin,HIGH);

    digitalWrite(cePin,HIGH);
}
